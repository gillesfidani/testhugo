---
title: Introduction
type: docs
---

# Accueil

Bienvenue sur mon magnifique site de tests.

but : me former à l'utilisation d'<a href="https://gohugo.io/" target="_blank">Hugo</a>, un *framework* pour construire des sites webs statiques.

+ des info sur l'hébergement de sites *via* les `pages` de Gitlab, c'est [ici](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/).

+ des info sur le **thème** utilisé ici (Hugo-Book), voir le <a href="https://github.com/alex-shpak/hugo-book" target="_blank">github du projet</a>.
